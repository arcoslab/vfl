# vfl


[![pipeline status](https://gitlab.com/arcoslab/vfl/badges/master/pipeline.svg)](https://gitlab.com/arcoslab/vfl/commits/master)


VFL: Vector Field library.

It is a vector field library that allows you to contruct a more complex vector field by mixing simpler vector fields. Once you have your final vector field ready you can query a vector for a particular position.
